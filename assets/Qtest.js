QUnit.test('Testing Multiplication function with four sets of inputs', function (assert) {  
    assert.strictEqual(intrest(2,4,2), 16, 'All positive numbers');
    assert.strictEqual(intrest(3,-6,2), -36, 'Positive and negative numbers');
    assert.strictEqual(intrest(3,-6,-2), 36, 'Positive and negative numbers');
    assert.strictEqual(intrest(-4,-8,-2), -64, 'All are negative numbers');
});